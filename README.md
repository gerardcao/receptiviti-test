#### Prerequisites 

-PHP 5.6+
-Composer (https://getcomposer.org/download/ - Follow this link to install if you do not have it)

#### 1. Clone the repository

```
git clone https://bitbucket.org/gerardcao/receptiviti-test.git
```

##### 2. Install components with composer from the root project directory

```
composer install
```

#### 3. All the use cases train are in ./tests/Recetiviti/TrainCase/TrainTest.php

#### 4. Launch the test 

```
./bin/phpunit
```