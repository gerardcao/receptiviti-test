<?php

namespace Tests\Receptiviti\TrainCase;

use Fhaculty\Graph\Graph;
use Receptiviti\Calculator\Distance;

/**
 * Class TrainTest
 */
class TrainTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Graph
     */
    private $graph;
    private $vertexs = array();

    public function setUp()
    {
        parent::setUp();

        $this->graph = new Graph();

        $this->vertexs['a'] = $this->graph->createVertex(1);
        $this->vertexs['b'] = $this->graph->createVertex(2);
        $this->vertexs['c'] = $this->graph->createVertex(3);
        $this->vertexs['d'] = $this->graph->createVertex(4);
        $this->vertexs['e'] = $this->graph->createVertex(5);
        ///AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7
        $this->vertexs['a']->createEdgeTo($this->vertexs['b'])->setWeight(5);
        $this->vertexs['b']->createEdgeTo($this->vertexs['c'])->setWeight(4);
        $this->vertexs['c']->createEdgeTo($this->vertexs['d'])->setWeight(8);
        $this->vertexs['d']->createEdgeTo($this->vertexs['c'])->setWeight(8);
        $this->vertexs['d']->createEdgeTo($this->vertexs['e'])->setWeight(6);
        $this->vertexs['a']->createEdgeTo($this->vertexs['d'])->setWeight(5);
        $this->vertexs['c']->createEdgeTo($this->vertexs['e'])->setWeight(2);
        $this->vertexs['e']->createEdgeTo($this->vertexs['b'])->setWeight(3);
        $this->vertexs['a']->createEdgeTo($this->vertexs['e'])->setWeight(7);
    }

    /**
     * 1.
     */
    public function testDistanceRouteAC()
    {
        $this->assertEquals(9, Distance::sumDistanceFromVertexs(array($this->vertexs['a'], $this->vertexs['b'], $this->vertexs['c'])));
    }

    /**
     * 2.
     */
    public function testDistanceRouteAD()
    {
        $this->assertEquals(5, Distance::sumDistanceFromVertexs(array($this->vertexs['a'], $this->vertexs['d'])));
    }

    /**
     * 3.
     */
    public function testDistanceRouteADC()
    {
        $this->assertEquals(13, Distance::sumDistanceFromVertexs(array($this->vertexs['a'], $this->vertexs['d'], $this->vertexs['c'])));
    }

    /**
     * 4.
     */
    public function testDistanceRouteAEBCD()
    {
        $this->assertEquals(22, Distance::sumDistanceFromVertexs(array($this->vertexs['a'], $this->vertexs['e'], $this->vertexs['b'], $this->vertexs['c'], $this->vertexs['d'])));
    }

    /**
     * 5.
     */
    public function testDistanceRouteAED()
    {
        //IF 0 IT MEANS NO SUCH ROUTE
        $this->assertEquals(0, Distance::sumDistanceFromVertexs(array($this->vertexs['a'], $this->vertexs['e'], $this->vertexs['d'])));
    }

    /**
     * 8.
     */
    public function testShortestRouteAC()
    {
        $this->assertEquals(9, Distance::getShortestDistanceFromVertexToVertex($this->vertexs['a'], $this->vertexs['c']));
    }

    /**
     * 9.
     */
    public function testShortestRouteBB()
    {
        $this->assertEquals(9, Distance::getShortestDistanceFromVertexToVertex($this->vertexs['b'], $this->vertexs['b']));
    }
}