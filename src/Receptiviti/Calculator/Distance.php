<?php

namespace Receptiviti\Calculator;

use Fhaculty\Graph\Set\Edges;
use Fhaculty\Graph\Vertex;
use Graphp\Algorithms\ShortestPath\MooreBellmanFord;

/**
 * Class Distance
 * @package Receptiviti\Calculator
 */
class Distance
{
    /**
     * @param Vertex $beginVertex
     * @param Vertex $endVertex
     * @return int
     */
    public static function getDistanceFromVertexToVertex(Vertex $beginVertex, Vertex $endVertex)
    {
        $totalDistance = 0;

        if ($endVertex->getGraph() === $beginVertex->getGraph()) {
            $edges = $beginVertex->getEdgesTo($endVertex);

            /** @var Edges $edges */
            foreach ($edges as $edge) {
                $totalDistance += $edge->getWeight();
            }
        }

        return $totalDistance;
    }

    /**
     * @param Vertex $beginVertex
     * @param Vertex $endVertex
     * @return float|null
     */
    public static function getShortestDistanceFromVertexToVertex(Vertex $beginVertex, Vertex $endVertex)
    {
        if ($endVertex->getGraph() === $beginVertex->getGraph()) {
            $alg = new MooreBellmanFord($beginVertex);
            return $alg->getDistance($endVertex);
        }

        return null;
    }

    /**
     * @param array $vertexs
     * @return int
     */
    public static function sumDistanceFromVertexs(array $vertexs)
    {
        $totalDistance = 0;
        $count = 1;

        foreach ($vertexs as $key => $vertex) {
            if (array_key_exists($key + 1, $vertexs)) {
                $distance = self::getDistanceFromVertexToVertex($vertex, $vertexs[$key + 1]);
                if ($distance > 0) {
                    $totalDistance += $distance;
                    $count++;
                }
            }
        }

        if ($count !== count($vertexs))
            return 0;

        return $totalDistance;
    }
}